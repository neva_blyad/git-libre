#    Makefile
#    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                            <neva_blyad@lovecri.es>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

.ONESHELL:
.PHONY: all vim install clean

all:

vim:
	touch .vim

install:
	git config --global color.branch.current                 154
	git config --global color.branch.local                       normal
	git config --global color.branch.remote                  196
	git config --global color.branch.upstream                196
	git config --global color.branch.plain                       normal
	
	git config --global color.status.added                   154
	git config --global color.status.updated                 154
	git config --global color.status.changed                 196
	git config --global color.status.untracked               196
	
	#git config --global color.diff.context                   normal
	git config --global color.diff.meta                      154
	git config --global color.diff.frag                      154 bold
	git config --global color.diff.func                      163
	git config --global color.diff.old                       196
	git config --global color.diff.new                        56
	
	git config --global color.decorate.branch                    normal
	git config --global color.decorate.remoteBranch              normal
	git config --global color.decorate.tag                       normal
	git config --global color.decorate.stash                     normal
	git config --global color.decorate.HEAD                      normal
	git config --global color.decorate.grafted                   normal
	
	test -f .vim && sudo update-alternatives --install /usr/bin/pager pager /usr/share/vim/vim*/macros/less.sh 104 && \
	                                                                                                                  \
	                git config --global core.editor     vim                                                        && \
	                git config --global core.pager      cat                                                        && \
	                git config --global diff.tool       vimdiff                                                    && \
	                git config --global difftool.prompt false
	true

clean:
	rm .vim
